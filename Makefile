all: draft-mavrogiannopoulos-pkcs5-passwords.txt draft-mavrogiannopoulos-pkcs5-passwords.html

clean:
	rm -f *~ \
	draft-mavrogiannopoulos-pkcs5-passwords.txt draft-mavrogiannopoulos-pkcs5-passwords.html

draft-mavrogiannopoulos-pkcs5-passwords.txt: draft-mavrogiannopoulos-pkcs5-passwords.xml
	xml2rfc $<

draft-mavrogiannopoulos-pkcs5-passwords.html: draft-mavrogiannopoulos-pkcs5-passwords.xml
	xml2rfc --html $<

